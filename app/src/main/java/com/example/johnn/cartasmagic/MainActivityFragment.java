package com.example.johnn.cartasmagic;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private ArrayList<String> items;
    private ArrayAdapter<String> adapter;


    public MainActivityFragment() {
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        ListView lvPelis = (ListView) view.findViewById(R.id.lvPelis);

        String[] data = {
//datas se cojen

                "Los 400 golpes",
                "El odio",
                "El padrino",
                "El padrino. Parte II",
                "Ocurrió cerca de su casa",
                "Infiltrados",
                "Umberto D."
        };

                items = new ArrayList<>(Arrays.asList(data));
                adapter = new ArrayAdapter<>(
                getContext(),
                R.layout.lista_cartas_row,
                R.id.lvpelis,
                items
        );

        lvPelis.setAdapter(adapter);



        return view;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_refresh, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id=item.getItemId();

        if(id == R.id.refresh){
            Refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void Refresh(){
        RefreshDataTask task = new RefreshDataTask();
        task.execute();

    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Cartas>> {

        @Override
        protected ArrayList<Cartas> doInBackground(Void... voids) {
            CartasAPI api = new CartasAPI();

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            String cartas_nombre = preferences.getString("cartas_nombre","Adorable Kitten");
            String tipo_consulta = preferences.getString("rarity_options","");

            ArrayList<Cartas> result = null;
            if(tipo_consulta!=""){
                result=api.getCartasTemagicByRarity(tipo_consulta);
            }
            else{
                result=api.getCartasTemagic();
            }

            //aqui pasan los datos de la api y los reproduce en el run
            //Log.d("DEBUG", result.toString());

            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<Cartas> cartas) {
            adapter.clear();
            for (Cartas carta : cartas) {
                adapter.add(carta.getName());

            }

        }


    }

}
