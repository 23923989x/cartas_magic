package com.example.johnn.cartasmagic;

import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class CartasAPI {

    private final String BASE_URL = "https://api.magicthegathering.io";

    ArrayList<Cartas> getCartasTemagic(){
        Uri builtUri=Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("v1")
                .appendPath("cards")
                .build();
        String url =builtUri.toString();

        return doCall(url);

    }

    ArrayList<Cartas> getCartasTemagicByRarity(String rarity){
        Uri builtUri=Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("v1")
                .appendPath("cards")
                .appendQueryParameter("rarity",rarity)
                .build();
        String url =builtUri.toString();

        return doCall(url);

    }



    private ArrayList<Cartas> doCall(String url){
        try {
            String JsonResponse = HttpUtils.get(url);
            return processJson(JsonResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Cartas> processJson(String jsonResponse){
        ArrayList<Cartas> cartas = new ArrayList<>();
        try {
            JSONObject data = new JSONObject(jsonResponse);
            JSONArray jsonMovies = data.getJSONArray("cards");
            for (int i = 0; i < jsonMovies.length(); i++) {
                JSONObject jsonMovie = jsonMovies.getJSONObject(i);

                Cartas carta = new Cartas();
                carta.setName(jsonMovie.getString("name"));
                carta.setType(jsonMovie.getString("type"));
                carta.setColors(jsonMovie.getString("colors"));
                carta.setImageUrl(jsonMovie.getString("imageUrl"));
               carta.setRarity(jsonMovie.getString("rarity"));

                cartas.add(carta);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return cartas;
    }


}
